package com.oreillyauto.domain;

public class OrlyTable {
    
    private String id;
    private String joke;
    
    
    public OrlyTable() {
        super();
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJoke() {
        return joke;
    }
    public void setJoke(String joke) {
        this.joke = joke;
    }

    /*@Override
    public String toString() {
        return "OrlyTable [id=" + id + ", joke=" + joke + "]";
    }*/

}

