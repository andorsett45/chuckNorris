package com.oreillyauto.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "EVENTS")
public class Event implements Serializable{

    private static final long serialVersionUID = -4513604167770036149L;
    
    //Constructors
    public Event() {
        super();
    }
    
    //Variables
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "EVENT_ID", columnDefinition = "INTEGER")
    private Integer eventId;
    
    @Column(name = "DATE_TIME", columnDefinition = "TIMESTAMP")
    private Timestamp dateTime;
    
    @Column(name = "EVENT_TYPE", columnDefinition = "VARCHAR(50)")
    private String eventType;
    
    @Column(name = "SMS_SENT", columnDefinition = "VARCHAR(1)")
    private String smsSent;
    
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "event", cascade = CascadeType.ALL)
    List<EventProperty> myEventList = new ArrayList<EventProperty>();
    
    
    //Getters & Setters
    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public Timestamp getDateTime() {
        return dateTime;
    }

    public void setDateTime(Timestamp dateTime) {
        this.dateTime = dateTime;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getSmsSent() {
        return smsSent;
    }

    public void setSmsSent(String smsSent) {
        this.smsSent = smsSent;
    }

    public List<EventProperty> getMyEventList() {
        return myEventList;
    }

    public void setMyEventList(List<EventProperty> myEventList) {
        this.myEventList = myEventList;
    }
    
    
    //ToString
    @Override
    public String toString() {
        return "Event [eventId=" + eventId + ", dateTime=" + dateTime + ", eventType=" + eventType + ", smsSent=" + smsSent + "]";
    }
        

}

