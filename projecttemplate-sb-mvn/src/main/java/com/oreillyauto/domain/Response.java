package com.oreillyauto.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Response implements Serializable {
    private static final long serialVersionUID = 7092938701510329645L;

    public Response() {
    }

    private String message;
    private String messageType;
    private List<OrlyTable> eventList = new ArrayList<OrlyTable>();
    //private List<Pupil> pupilList = new ArrayList<Pupil>();
    
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public List<OrlyTable> getEventList() {
        return eventList;
    }

    public void setEventList(List<OrlyTable> eventList) {
        this.eventList = eventList;
    }
    
}
