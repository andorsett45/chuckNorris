package com.oreillyauto.domain;

import java.util.Arrays;

public class SmsData {

    private String phoneNumber;
    private Person [] events;
    
    public SmsData() {
        super();
    }
    public SmsData(String phoneNumber, Person[] events) {
        super();
        this.phoneNumber = phoneNumber;
        this.events = events;
    }
    public String getPhoneNumber() {
        return phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    public Person[] getEvents() {
        return events;
    }
    public void setEvents(Person[] events) {
        this.events = events;
    }
    @Override
    public String toString() {
        return "SmsData [phoneNumber=" + phoneNumber + ", events=" + Arrays.toString(events) + "]";
    }
    
    
}
