package com.oreillyauto.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.oreillyauto.service.TestService;

@Controller
public class TestController {
	
	@Autowired
	TestService testService;
	
    @RequestMapping(value = "/test")
    public String home(Model model) {
    	model.addAttribute("dbTime", testService.getDatabaseTimestamp());
        return "test";
    }
    
}
