package com.oreillyauto.service.impl;

import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.oreillyauto.dao.FinalProjectPropRepository;
import com.oreillyauto.dao.FinalProjectRepository;
import com.oreillyauto.domain.ChuckNorris;
import com.oreillyauto.domain.Event;
import com.oreillyauto.domain.EventProperty;
import com.oreillyauto.domain.OrlyTable;
import com.oreillyauto.domain.Person;
import com.oreillyauto.domain.SmsData;
import com.oreillyauto.service.FinalProjectService;
import com.oreillyauto.util.TwilioUtil;
import com.twilio.Twilio;
import com.twilio.exception.ApiException;
import com.twilio.rest.lookups.v1.PhoneNumber;

@Service("finalProjectService")
public class FinalProjectServiceImpl implements FinalProjectService{
    
    @Autowired FinalProjectRepository finalProjectRepo;
    @Autowired FinalProjectPropRepository finalProjectPropRepo;

    @Override
    public List<OrlyTable> getEvents() {
        List<Event> allEvents = (List<Event>) finalProjectRepo.findAll();

        List<OrlyTable> orlyTable = new ArrayList<OrlyTable>();

        for (Event event : allEvents) {
            OrlyTable data = new OrlyTable(); //create an instance

            List<EventProperty> eventList = event.getMyEventList();
            for (EventProperty eventProp : eventList) {             //loop through eventProperties
                if (eventProp.getEventKey().equals("id")) {
                    data.setId(eventProp.getEventValue());
                }
                if (eventProp.getEventKey().equals("value")) {
                    data.setJoke(eventProp.getEventValue());
                }
            }
            orlyTable.add(data);
        }
        return orlyTable;

    }

    @Override
    public boolean saveEvent() {
        String url = "https://matchilling-chuck-norris-jokes-v1.p.rapidapi.com/jokes/random";

        // Setup authentication and encode it
        String auth = "7db201b928mshd5d2ab70e9948b6p1f891cjsn959f0edea9dc";
        byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));

        // Create Request Headers
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.put("x-rapidapi-host", Arrays.asList("matchilling-chuck-norris-jokes-v1.p.rapidapi.com"));
        headers.put("x-rapidapi-key", Arrays.asList("7db201b928mshd5d2ab70e9948b6p1f891cjsn959f0edea9dc"));
        headers.put("accept", Arrays.asList("application/json"));
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        headers.set("Authorization", "Basic " + new String(encodedAuth));

        // Create Request Body (Payload) (if applicable)
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();

        // Send The Request to the Web Service and Print the Response
        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(params, headers);
        RestTemplate restTemplate = new RestTemplate();
        //ResponseEntity<String> response = restTemplate.getForEntity(url, entity, String.class);
        ResponseEntity<ChuckNorris> response = restTemplate.exchange(url, HttpMethod.GET, entity, ChuckNorris.class);
        ChuckNorris chuckNorris = response.getBody(); 
        
        //SAVING THE DATA TO DATABASE --> Making key,value pairs between Joke and EventProp and EventProp and Event
        Event event = new Event(); // Create a new instance of the Event object
        Date date = new Date();
        event.setDateTime(new Timestamp(date.getTime()));
        event.setEventType("joke");
 
        // Create a list of type Event Property
        List<EventProperty> eventPropList = new ArrayList<EventProperty>(); 

        //Create new instances of event property
        EventProperty eventPropId = new EventProperty();
        EventProperty eventPropContent = new EventProperty();
        
        //Values for Content from Joke
        eventPropId.setEventKey("id");
        eventPropId.setEventValue(chuckNorris.getId());
        eventPropId.setGroupId(chuckNorris.getId());
        eventPropId.setEvent(event);
        eventPropList.add(eventPropId);
        
        //Values for Content from Joke
        eventPropContent.setEventKey("value");
        eventPropContent.setEventValue(chuckNorris.getValue());
        eventPropContent.setGroupId(chuckNorris.getId());
        eventPropContent.setEvent(event);
        eventPropList.add(eventPropContent);
        
        event.setMyEventList(eventPropList); // Set the event property list on the event object

        
        Event savedEvent = finalProjectRepo.save(event); //Save the event and return the event that comes back
        return savedEvent != null;

    }
    //Updating to set SMS Sent as 1
    @Override
    public void setSMSSent(List<Person> personList) {
        List<String> groupList = new ArrayList<String>();
       
        for (Person person : personList) {
            groupList.add(person.getId());
        }
        List<EventProperty> propList = finalProjectRepo.getByGroupIds(groupList);
        for (EventProperty eventProperty : propList) {
            eventProperty.setSmsSent("1");
        }
        finalProjectPropRepo.saveAll(propList);
    }

    @Override
    public String sendSMS(SmsData data) throws InterruptedException {
        final String ACCOUNT_SID = "ACeeba5d11271d7587c803d52205f973ba";
        final String AUTH_TOKEN = "2b8ac5807e19ac44b428d0202b433278";
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        PhoneNumber number = PhoneNumber
                .fetcher(new com.twilio.type.PhoneNumber(data.getPhoneNumber()))
                .setType(Arrays.asList("carrier"))
                .fetch();
        //System.out.println(number.getCarrier());
        if (number.getCarrier().get("type") == "voip") {
            String errMessage = "The number you entered is a voip. Only support mobile numbers";
            //orly.qid("alerts").createAlert({type:"danger", duration:"3000", msg:errMessage});
            throw new Error(errMessage);
        }
        if (number.getCarrier().get("type") == "landline") {
            String errMessage = "The number you entered is a landline. Only support mobile numbers";
            //orly.qid("alerts").createAlert({type:"danger", duration:"3000", msg:errMessage});
            throw new Error(errMessage);
        }
    
        Person[] myPeeps = data.getEvents();
        try {
            for (Person person : myPeeps) {
                new TwilioUtil().sendSms(data.getPhoneNumber(), person.getMessage());
            }
        } catch(Exception e){
            return e.getMessage();
        }
        
        setSMSSent(Arrays.asList(myPeeps));
        return "Your SMS message(s) have been sent successfully.";
    }

}
