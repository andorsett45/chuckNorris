package com.oreillyauto.service;

import java.util.List;

import com.oreillyauto.domain.Event;
import com.oreillyauto.domain.OrlyTable;
import com.oreillyauto.domain.Person;
import com.oreillyauto.domain.SmsData;

public interface FinalProjectService {

    public List<OrlyTable> getEvents();
    
    public boolean saveEvent();

    public void setSMSSent(List<Person> personList);

    public String sendSMS(SmsData data) throws InterruptedException;
}
