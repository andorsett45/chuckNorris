package com.oreillyauto.dao;

import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.FinalProjectPropRepositoryCustom;
import com.oreillyauto.domain.EventProperty;

public interface FinalProjectPropRepository extends CrudRepository<EventProperty, Integer>, FinalProjectPropRepositoryCustom{

    

}
