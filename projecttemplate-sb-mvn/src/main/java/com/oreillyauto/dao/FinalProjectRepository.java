package com.oreillyauto.dao;

import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.FinalProjectRepositoryCustom;
import com.oreillyauto.domain.Event;

public interface FinalProjectRepository extends CrudRepository<Event, Integer>, FinalProjectRepositoryCustom{

    

}
