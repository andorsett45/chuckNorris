package com.oreillyauto.dao.custom;

import java.util.List;

import com.oreillyauto.domain.Event;
import com.oreillyauto.domain.EventProperty;

public interface FinalProjectRepositoryCustom {

    public List<Event> getEvents();
    List<EventProperty> getByGroupIds(List<String> groupList);
}
